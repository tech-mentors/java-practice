package com.basic.stack;

import com.basic.data.structure.StackAsLinkedList;

public class StackAsLinkedListTest {
  // Driver code
  public static void main(String[] args) {

    com.basic.data.structure.StackAsLinkedList sll = new StackAsLinkedList();
    System.out.println("Stack is empty::" + sll.isEmpty());
    sll.push(10);
    sll.push(20);
    sll.push(30);

    System.out.println(sll.pop()
        + " popped from stack");
    System.out.println("Stack is empty::" + sll.isEmpty());
    sll.push(22);
    System.out.println("Top element is " + sll.peek());
  }
}
