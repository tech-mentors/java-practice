package com.basic.stack;

// Java Code for Linked List Implementation

//Time Complexities of operations on stack:
//
//push(), pop(), isEmpty() and peek() all take O(1) time. We do not run any loop in any of these operations.
//
//Applications of stack:
//
//Balancing of symbols
//Infix to Postfix /Prefix conversion
//Redo-undo features at many places like editors, photoshop.
//Forward and backward feature in web browsers
//Used in many algorithms like Tower of Hanoi, tree traversals, stock span problem, histogram problem.
//Other applications can be Backtracking, Knight tour problem, rat in a maze, N queen problem and sudoku solver
//In Graph Algorithms like Topological Sorting and Strongly Connected Components

import com.basic.data.structure.LinkedList;

import static com.basic.utils.LinkListUtil.nullSafeInsertInList;
import static com.basic.utils.PrintUtils.printlnStr;

final class Stack1 {
  LinkedList linkedList;

  public final boolean isEmpty() {
    return linkedList == null || linkedList.isEmpty();
  }

  public final void push(int data) {
    linkedList = nullSafeInsertInList(linkedList, data);
    printlnStr(data + " pushed in stack.");
  }

  public int pop() {
    int popped = Integer.MIN_VALUE;
    if (linkedList.head == null) {
      System.out.println("StackWithArray is Empty");
    } else {
      popped = linkedList.head.data;
      linkedList.head = linkedList.head.next;
    }
    return popped;
  }

  public int peek() {
    if (linkedList.head == null) {
      System.out.println("StackWithArray is empty");
      return Integer.MIN_VALUE;
    } else {
      return linkedList.head.data;
    }
  }
}

public class StackAsLinkedList {

  public static void main(String[] args) {
    Stack1 sll = new Stack1();
    printlnStr("stack is empty is " + sll.isEmpty());
    sll.push(10);
    sll.push(20);
    sll.push(30);
    printlnStr("stack is empty is " + sll.isEmpty());
    System.out.println(sll.pop() + " popped from stack");
    System.out.println("Top element is " + sll.peek());
  }
}
