package com.basic;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ArrTest {
  public static void main(String[] args) {
    int[] arr = {1, 2, 4, 2, 1, 1};
    int count = countPairInArray(arr);
    System.out.println("count = " + count);


  }

  public int[] convertListToArray(List<Integer> list) {
    return  list.stream().mapToInt(Integer::intValue).toArray();
  }

  public static int countPairInArray(int[] arr) {
    return (int) Arrays.stream(arr)
        .boxed()
        .collect(Collectors.toMap(k -> k, v -> 1, Integer::sum))
        .values()
        .stream()
        .filter(integer -> integer >= 2)
        .count();
  }
}
