package com.basic.stream.api;
// Java code to show implementation
// of limit() function
import java.util.*;
import java.util.stream.*;

public class StreamLimit {



    // Function to limit the stream upto given range, i.e, 3
    public static Stream<Object> limiting_func(Stream<Object> ss, int range){
      return ss.limit(range);
    }

    // Driver code
    public static void main(String[] args){

      // list to save stream of strings
      List arr = new ArrayList();

      arr.add("geeks0");
      arr.add("for1");
      arr.add(12);
      arr.add("geeks2");
      arr.add("computer3");
      arr.add("science4");

      Stream<Object> str = arr.stream();

      // calling function to limit the stream to range 3
      Stream<Object> lm = limiting_func(str,3);
      lm.forEach(System.out::println);
    }
  }
