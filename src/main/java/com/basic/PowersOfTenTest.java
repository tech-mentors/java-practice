package com.basic;

public class PowersOfTenTest {

  public static boolean isPowerOfTen(long input) {
    while (input >= 10 && input % 10 == 0)
      input /= 10;
    return input == 1;
  }

  public static void main(String[] args) {

    System.out.println("1000: " + isPowerOfTen(1000));
    System.out.println("4: " + isPowerOfTen(4));
    System.out.println("0: " + isPowerOfTen(0));
    System.out.println("10: " + isPowerOfTen(10));
    System.out.println("100: " + isPowerOfTen(100));
    System.out.println("500: " + isPowerOfTen(500));
    System.out.println("50: " + isPowerOfTen(50));
  }
}