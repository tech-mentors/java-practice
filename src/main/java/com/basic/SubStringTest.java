package com.basic;

public class SubStringTest {
  public static void main(String[] args) {
    String str = "aabbcc";
    System.out.print("[");
    for (int i = 0; i < str.length(); i++) {
      for (int j = i + 1; j <= str.length(); j++) {
        System.out.print(str.substring(i, j)+",");
      }
    }
    System.out.println("]");
  }
}
