package com.basic;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class Employee {
  private String grade;
  private String name;
  private Double salary;
  private int id;

  public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Double getSalary() {
    return salary;
  }

  public void setSalary(Double salary) {
    this.salary = salary;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Employee(int id, String name, Double salary, String grade) {
    this.grade = grade;
    this.name = name;
    this.salary = salary;
    this.id = id;
  }

  public Employee() {
    this(0, null, null, null);
  }

  @Override public String toString() {
    return String.format("\nId = %s,Name = %s,Salary = %s,Grade = %s",
        this.id,
        this.name,
        this.salary,
        this.grade);
  }
}

public class ReplaceInsideStreamTest {

  public static void main(String[] as) {
    List<Employee> employees = new ArrayList<>(10);
    employees.add(new Employee(1, "sanjeev", 40000D, "C"));
    employees.add(new Employee(2, "Rajeev", 85000D, "B"));
    employees.add(new Employee(3, "Ranjeet", 30000D, "C"));
    employees.add(new Employee(4, "sanjeet", 46000D, "C"));
    System.out.println("Before changes emp list is::" + employees.toString());
   employees= employees.stream()
        .peek(e ->{ if(e.getSalary().compareTo(45000D) > 0) e.setGrade("A");}).collect(Collectors.toList());

    System.out.println("After changes emp list is::" + employees.toString());
  }
}
