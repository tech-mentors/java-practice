package com.basic.seaching;

import com.basic.sorting.MergeSort;
import java.util.Arrays;

public class SearchingTest {
  // Driver code
  public static void main(String[] args) {
    int[] arr = {2, 39, 4, 10, 98, 86, 97, 54, 8, 40};
    int x = 10;
    System.out.println(
        "Given array is:: = " + Arrays.toString(arr) + " and element search is::  " + x);
    // Function call

    //int result = LinearSearch.search(arr, x);

    //binary search
    int[] sortedArr = MergeSort.sort(arr);
    System.out.println(
        "Sorted array is:: = " + Arrays.toString(sortedArr) + " and element search is::  " + x);
    int result = BinarySearch.search(sortedArr, x);

    if (result == -1) {
      System.out.print(
          "Element is not present in array");
    } else {
      System.out.print("Element is present at index "
          + result);
    }
  }
}
