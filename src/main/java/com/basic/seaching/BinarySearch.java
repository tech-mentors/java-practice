package com.basic.seaching;

// Java code for linearly searching x in arr[]. If x
// is present then return its location, otherwise
// return -1

import com.basic.sorting.MergeSort;

public class BinarySearch {
  public static int search(int[] arr, int x) {
    //sort first array because binary search applied only on sorted array
    return binarySearch(MergeSort.sort(arr), x);
  }

  private static int binarySearch(int[] sortedArr, int x) {
    int len = sortedArr.length;
    int foundIndex = -1;
    int left = 0, right = len - 1;
    while (left <= right) {
      int middle = left + (right - left) / 2;
      if (sortedArr[middle] < x) {
        left = middle + 1;
      } else if (sortedArr[middle] > x) {
        right = middle - 1;
      } else {
        foundIndex = middle;
        break;
      }
    }
    return foundIndex;
  }
}

