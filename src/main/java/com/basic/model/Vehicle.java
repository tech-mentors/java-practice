package com.basic.model;

public class Vehicle {

  private String name;
  private int price;

  public Vehicle(String name, int price) {
    this.name = name;
    this.price = price;
  }

  @Override public String toString() {
    return "Vehicle{" +
        "name='" + name + '\'' +
        ", price=" + price +
        '}';
  }

  @Override public int hashCode() {
    return this.price + this.name.hashCode();
  }

  @Override public boolean equals(Object obj) {
    if (obj instanceof Vehicle) {
      Vehicle vehicle = (Vehicle) obj;
      return this.name.equals(vehicle.name) && this.price == vehicle.price;
    }
    return super.equals(obj);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }
}
