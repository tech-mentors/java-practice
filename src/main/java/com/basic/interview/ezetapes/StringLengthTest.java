package com.basic.interview.ezetapes;

import java.math.BigInteger;

public class StringLengthTest {
  public static final int END=Integer.MAX_VALUE;
  public static final int START=END-100;
  public static void main(String[] args) {
    
    //question 1
    final String pig="length: 10";
    System.out.println("pig = " + pig);
    final String dog="length: "+pig.length();
    System.out.println("dog = " + dog);
    System.out.println("Animal is same = " + (dog==pig));
    System.out.println("Animal is same = " + (dog.equals(pig)));
    System.out.println("Animal is same = " + dog==pig);

   //question 2
    
    int count=0;
    //for(int i=START;i<=END;i++){
    //  count++;
    //}
    System.out.println("count = " + count);

    //Question 3
    BigInteger b1=new BigInteger("5000");
    BigInteger b2=new BigInteger("50000");
    BigInteger b3=new BigInteger("500000");
    BigInteger tot=BigInteger.ZERO ;
    tot.add(b1);
    tot.add(b2);
    tot.add(b3);
    System.out.println("tot = " + tot);


    /*
    compile time exception because io exception is never thrown
    try {
      System.out.println("StringPalindromeUsingRecursionTest io exception.");
    }catch (IOException exception){
      System.out.println("I will not call any where.");
    }
    */
  }

}
