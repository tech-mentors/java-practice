package com.basic.interview.ezetapes;

import java.util.Arrays;

public class StringToArray {
  //question 7
  public static void main(String[] args) {
    String input = "10,2,9,6,3,16,7";
    String sumStr = "12";
    int[] ss = stringToIntArray(input, false);
    System.out.println("output = " + checkSumPairInArray(ss, Integer.parseInt(sumStr)));
    ss = stringToIntArray(input, true);
    System.out.println("output = " + checkSumPairInArray2(ss, Integer.parseInt(sumStr)));
  }

  private static int[] stringToIntArray(String input, boolean isSorted) {
    if (isSorted) {
      return Arrays.stream(input.split(","))
          .mapToInt(value -> Integer.parseInt(value.trim()))
          .sorted()
          .toArray();
    } else {
      return Arrays.stream(input.split(","))
          .mapToInt(value -> Integer.parseInt(value.trim()))
          .toArray();
    }
  }

  private static String checkSumPairInArray(int[] ss, int sum) {
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < ss.length - 1; i++) {
      for (int j = (i + 1); j < ss.length; j++) {
        if (ss[i] + ss[j] == sum) {
          if (builder.length() == 0) {
            builder.append("true\n");
          } else {
            builder.append(",");
          }
          builder.append(ss[i]).append(",").append(ss[j]);
        }
      }
    }
    return builder.toString();
  }

  private static String checkSumPairInArray2(int[] ss, int sum) {
    StringBuilder builder = new StringBuilder();
    int l = 0, r = ss.length - 1;
    while (l < r) {
      if (ss[l] + ss[r] == sum) {
        if (builder.length() == 0) {
          builder.append("true\n");
        } else {
          builder.append(",");
        }
        builder.append(ss[l]).append(",").append(ss[r]);
        l++;
        r++;
      } else if (ss[l] + ss[r] < sum) {
        l++;
      } else // ss[i] + ss[j] > sum
      {
        r--;
      }
    }
    return builder.toString();
  }
}
