package com.basic;

import java.util.ArrayList;
import java.util.List;

public class CallByReferenceTest {
  public static void main(String[] args) {
    List<String> strings = new ArrayList<>();
    strings.add("one");
    strings.add("two");
    strings.add("three");
    strings.add("four");
    System.out.println("strings = " + strings);
    alterList(strings);
    System.out.println("strings = " + strings);
  }

  private static void alterList(List<String> temp) {
    /*
    if we assign new object then it will not affect parameter temp because it will referer to another point.
     temp=new ArrayList<>(0);
    */

    // it will point same memory location so if you change anything in same memory location it will affect.
    temp.remove(0);
  }
}
