package com.basic.strings;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class StringUtils {

  private StringUtils() {
    throw new IllegalStateException("can't call this");
  }

  public static Map<Character, Integer> distinctCharCountMap(String str) {
    return str.chars()
        .boxed()
        .collect(Collectors.toMap(
            k -> (char) k.intValue(),
            v -> 1,
            Integer::sum,
            LinkedHashMap::new));
  }

  public static int distinctCharCountOnly(String str) {
    return (int) str.chars()
        .boxed()
        .distinct().count();
  }

  public static void main(String[] args) {
    System.out.println("distinctCharCountOnly(\"rajujha\") = " + distinctCharCountOnly("rajujha"));
    System.out.println("distinctCharCountOnly(\"rajujha\") = " + distinctCharCountMap("rajujha"));
  }
}
