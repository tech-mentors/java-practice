package com.basic.strings;

public class PatternPrinterTest {
  /*
  *  input - 2A3B4F
  *  output - AABBBFFFF
  *
  *
  * */
  public static String printMe(String str) {
    int n = str.length();
    StringBuilder output = new StringBuilder();
    for (int i = 0; i < n; i += 2) {
      int c = Integer.parseInt(str.substring(i, i + 1));
      //System.out.println("c = " + c);
      String repeater=str.substring(i + 1, i + 2);
      output.append(repeater.repeat(c));
    }
    return output.toString();
  }
  // Driver Code
  public static void main(String[] args) {
    System.out.println("output is = " + printMe("2A3B4F"));
  }


}
