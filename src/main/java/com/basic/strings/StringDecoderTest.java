package com.basic.strings;

import java.util.Vector;

public class StringDecoderTest {
  public static void main(String[] args) {
    System.out.println(" decoded string1= " + decodeMe("3[a]2[bc]"));
    System.out.println(" decoded string 2= " + decodeMe("3[a2[c]]"));
    System.out.println(" decoded string 3= " + decodeMe("2[abc]3[cd]ef"));
  }

  private static String decodeMe(String s) {
    Vector<Character> st = new Vector<>();
    for (int i = 0; i < s.length(); i++) {
      if (s.charAt(i) != ']') {
        // If s[i] is not ']', simply push
        // s[i] to the stack
        st.add(s.charAt(i));
      } else {
        // When ']' is encountered, we need to
        // start decoding
        StringBuilder temp = new StringBuilder();

        for (int last = st.size() - 1; last > -1 && st.get(last) != '['; last = st.size() - 1) {
          // st.top() + temp makes sure that the
          // string won't be in reverse order eg, if
          // the stack contains 12[abc temp = c + "" =>
          // temp = b + "c" => temp = a + "bc"
          temp.insert(0, st.get(last));
          st.remove(last);
        }
        // Remove the '[' from the stack
        st.remove(st.size() - 1);

        StringBuilder num = new StringBuilder();
        // Remove the digits from the stack
        while (isLastElementNumeric(st)) {
          num.insert(0, st.get(st.size() - 1));
          st.remove(st.size() - 1);
        }
        addNumberByNTimesElementInStack(st, num, temp);
      }
    }

    return parseResultFromStack(st);
  }

  private static void addNumberByNTimesElementInStack(
      Vector<Character> st,
      StringBuilder num,
      StringBuilder temp) {
    int number = Integer.parseInt(num.toString());
    StringBuilder repeat = new StringBuilder();
    repeat.append(String.valueOf(temp).repeat(Math.max(0, number)));
    for (int c = 0; c < repeat.length(); c++)
      st.add(repeat.charAt(c));
  }

  private static boolean isLastElementNumeric(Vector<Character> st) {
    return st.size() > 0 &&
        st.get(st.size() - 1) >= 48 &&
        st.get(st.size() - 1) <= 57;
  }

  private static String parseResultFromStack(Vector<Character> st) {
    //parse result from stack
    StringBuilder res = new StringBuilder();
    while (st.size() > 0) {
      res.insert(0, st.get(st.size() - 1));
      st.remove(st.size() - 1);
    }
    // return result;
    return res.toString();
  }
}
