package com.basic.strings;

import static com.basic.strings.StringUtils.distinctCharCountOnly;

/**
 * https://www.geeksforgeeks.org/smallest-window-contains-characters-string/
 * */
public class FindSubStringContainAllCharItselfTest {
  // Function to find smallest window containing
  // all distinct characters
  private static String findSubString(String str) {
    int n = str.length();

    // Count all distinct characters.
    int dist_count = distinctCharCountOnly(str);
    int size = Integer.MAX_VALUE;
    String res = "";

    // Now follow the algorithm discussed in below
    for (int i = 0; i < n; i++) {
      int count = 0;
      //create array
      int[] visited = new int[256];
      //fill zero
      for (int j = 0; j < 256; j++)
        visited[j] = 0;
      StringBuilder sub_str = new StringBuilder();
      for (int j = i; j < n; j++) {
        char c = str.charAt(j);
        if (visited[c] == 0) {
          count++;
          visited[c] = 1;
        }
        sub_str.append(c);
        if (count == dist_count) {
          break;
        }
      }
      if (sub_str.length() < size && count == dist_count) {
        res = sub_str.toString();
        //size = res.length();
      }
    }
    return res;
  }

  // Driver code
  public static void main(String[] args) {
    String str = "nutan";
    System.out.println("Given string = " + str);
    System.out.println("Smallest window containing all distinct" +
        " characters is: " + findSubString(str));
  }
}