package com.basic.strings;

public class StringPalindromeUsingRecursionTest {

  public static void main(String[] args) {
    String aString = "madam";
    System.out.printf("Given aString '%s' is%s palindrome.", aString, checkMe(aString) ? "" : " not");
  }

  private static boolean checkMe(String str) {
    if (str == null || str.length() == 0 || str.trim().length() == 0) {
     // System.out.println("1 str = " + str);
      return false;
    } else if (str.length() == 1) {
      //System.out.println("2 str = " + str);
      return true;
    } else if (str.charAt(0) == str.charAt(str.length() - 1)) {
      //System.out.println("3 str = " + str);
      return str.length() == 2 || checkMe(str.substring(1, str.length() - 1));
    } else {
      return false;
    }
  }
}
