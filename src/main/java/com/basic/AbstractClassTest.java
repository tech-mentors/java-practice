package com.basic;

abstract class Printer {
  public abstract Number getValue() throws Exception;

  public void printMe() throws Exception {
    System.out.println("value = " + getValue());
  }
}

class SumPrinter extends Printer {

  @Override public Integer getValue() throws RuntimeException {
    return 0;
  }
}

public class AbstractClassTest {
  public static void main(String[] args) throws Exception {
    Printer p = new SumPrinter();
    p.printMe();
  }
}
