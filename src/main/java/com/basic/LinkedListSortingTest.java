package com.basic;

import com.basic.data.structure.LinkedList;

import static com.basic.utils.LinkListUtil.nullSafeInsertInList;
import static com.basic.utils.LinkListUtil.nullSafePrintList;

public class LinkedListSortingTest {
  public static void main(String[] args) {
    LinkedList linkedList = new LinkedList();
    nullSafeInsertInList(linkedList, 1);
    nullSafeInsertInList(linkedList, -2);
    nullSafeInsertInList(linkedList, -3);
    nullSafeInsertInList(linkedList, 4);
    nullSafeInsertInList(linkedList, -5);
    nullSafePrintList(linkedList, "Before Sort LinkedList = ");
    sortList(linkedList.head);
    nullSafePrintList(linkedList, "After sort LinkedList = ");
  }

  private static void sortList(LinkedList.Node head) {
    int startVal = 1;
    while (head != null) {
      head.data = startVal;
      startVal++;
      head = head.next;
    }
  }
}
