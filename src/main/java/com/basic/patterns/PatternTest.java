package com.basic.patterns;

import java.util.Scanner;

class Pattern {

  static void pattern1(int n) {
    for (int i = 1; i <= n; i++) {
      for (int k = n - i; k > 0; k--) {
        System.out.print("  ");
      }
      for (int j = 1; j <= 2 * i - 1; j++) {
        char c = (char) (j + 64);
        System.out.print(" " + c);
      }
      System.out.println(" ");
    }
  }
}

public class PatternTest {
  public static void main(String[] args) throws Exception {
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    Pattern.pattern1(n);
  }
}
