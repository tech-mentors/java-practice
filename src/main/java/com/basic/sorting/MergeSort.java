package com.basic.sorting;

public class MergeSort {
  private static void merge(int[] arr, int left, int middle, int right) {
    int low = middle - left + 1;                    //size of the left subarray
    int high = right - middle;                      //size of the right subarray
    int[] L = new int[low];                             //create the left and right subarray
    int[] R = new int[high];
    int i, j;
    //copy elements into left subarray
    for (i = 0; i < low; i++) {
      L[i] = arr[left + i];
    }
    //copy elements into right subarray
    for (j = 0; j < high; j++) {
      R[j] = arr[middle + 1 + j];
    }

    int k = left;                                           //get starting index for sort
    i = j = 0;
    //merge the left and right subarrays
    while (i < low && j < high) {
      arr[k++] = (L[i] <= R[j]) ? L[i++] : R[j++];
    }
    //merge the remaining elements from the left subarray
    while (i < low) {
      arr[k++] = L[i++];
    }
    //merge the remaining elements from right subarray
    while (j < high) {
      arr[k++] = R[j++];
    }
  }

  //helper function that creates the sub array for sorting
  private static void mergeSort(int[] arr, int left, int right) {
    if (left < right) {
      int middle = (left + right) / 2;
      mergeSort(arr, left, middle);                    //left subarray
      mergeSort(arr, (middle + 1), right);               //right subarray
      merge(arr, left, middle, right);                //merge the two subarrays
    }
  }

  public static int[] sort(int[] arr) {
    mergeSort(arr, 0, arr.length - 1);
    return arr;
  }
}
