package com.basic.sorting;

import java.util.Arrays;

public class SortingTestService {

  void display(int[] arr, String msg) {
    System.out.println(
        (msg == null || msg.length() == 0 ? "Array is :: = " : msg) + Arrays.toString(arr));
  }

  public static void main(String[] args) {
    int[] arr = {9, 3, 1, 5, 13, 12, -3};
    SortingTestService ob = new SortingTestService();
    ob.display(arr, "");
    //int[] sort = MergeSort.sort(arr);
    //int[] sort = SelectionSort.sort(arr);
    int[] sort = BubbleSort.sort(arr);
    ob.display(sort, "After sort array is ::");
  }
}
