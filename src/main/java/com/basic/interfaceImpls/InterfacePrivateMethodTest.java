package com.basic.interfaceImpls;

// Java 9 program to illustrate
// private methods in interfaces
interface Calculator {

  void mul(int a, int b);

  default void add(int a, int b) {
    // private method inside default method
    sub(a, b);
    // static method inside other non-static method
    div(a, b);
    System.out.print("Answer by Default method = ");
    System.out.println(a + b);
  }

  static void mod(int a, int b) {
    div(a, b); // static method inside other static method
    System.out.print("Answer by Static method = ");
    System.out.println(a % b);
  }

  private void sub(int a, int b) {
    System.out.print("Answer by Private method = ");
    System.out.println(a - b);
  }

  private static void div(int a, int b) {
    System.out.print("Answer by Private static method = ");
    System.out.println(a / b);
  }
}

class CalculatorImpl implements Calculator {
  @Override
  public void mul(int a, int b) {
    System.out.print("Answer by Abstract method = ");
    System.out.println(a * b);
  }
}

public class InterfacePrivateMethodTest {
  public static void main(String[] args) {
    Calculator in = new CalculatorImpl();
    in.mul(2, 3);
    in.add(6, 2);
    Calculator.mod(5, 3);
  }
}

