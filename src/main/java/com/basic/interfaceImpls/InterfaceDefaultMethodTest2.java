package com.basic.interfaceImpls;

// A simple Java program to StringPalindromeUsingRecursionTest Class  static
// methods in java
@FunctionalInterface
interface TestInterface {
  // abstract method

  public Number square(int a);

  // static method
  private static void show() {
    System.out.println("Static Method Executed");
  }

  // static method
  default void show1() {
    System.out.println("Default Method Executed");
  }
}

interface TestInterface1 {
  // abstract method
  public Number square(int a);

  // static method
  static void show() {
    System.out.println("Static Method Executed");
  }

  // static method
  default void show1() {
    System.out.println("Default Method Executed");
  }
}

class InterfaceDefaultMethodTest implements TestInterface, TestInterface1 {
  // Implementation of square abstract method
  public Integer square(int a) {
    System.out.println(a * a);
    return a * a;
  }

  static void show() {
    System.out.println("static Method Executed in child class");
  }

  @Override
  public void show1() {
    System.out.println("Default Method Executed in child class");
  }
}

public class InterfaceDefaultMethodTest2 extends InterfaceDefaultMethodTest {
  // Implementation of square abstract method
  public Integer square(int a) {
    System.out.println(a * a);
    return a * a;
  }

  public void show1() {
    System.out.println("Default Method Executed in sub child class");
  }

  public void fun() {
    System.out.println("only in sub child class");
  }

  public static void main(String[] args) {
    InterfaceDefaultMethodTest2 d = new InterfaceDefaultMethodTest2();
    d.square(4);

    // Static method executed
    //TestInterface.show();
    d.show();

    //default
    d.show1();
  }
}
