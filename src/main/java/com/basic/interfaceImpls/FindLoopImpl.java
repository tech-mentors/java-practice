package com.basic.interfaceImpls;

interface FindLoop {

  static void testLoop() {
    System.out.println("static method is called.");
  }

  default void testDefault() {
    System.out.println("default method1 is called.");
  }
}

public class FindLoopImpl implements FindLoop {

  public static void main(String[] args) {
    FindLoop impl = new FindLoopImpl();
    impl.testDefault();
    FindLoop.testLoop();
  }
}