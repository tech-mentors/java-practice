package com.basic;

import java.util.Arrays;

public class FindDuplicateTest {
  public static void main(String[] args) {
    // JAVA code to find
    // duplicates in O(n) time

    int[] numRay = {0,14,54,14, 4, 4,3, 2, 7, 8, 2, 3,4,4,4, 1};
   int len=numRay.length;
    for (int i = 0; i < len; i++) {
      var index =numRay[i] % len;
      numRay[index]  += len;
    }
    System.out.println("Arrays.toString(numRay = " + Arrays.toString(numRay));
    System.out.println("The repeating elements are : ");
    for (int i = 0; i < len; i++) {
      if (numRay[i] >= len * 2) {
        System.out.println(i + " ");
      }
    }
  }
}



