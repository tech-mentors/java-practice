package com.basic.functionalInterface;

public class MainTest {

  public static void main(String[] args) {
    Shape s = new Rectangle();
    s.draw();
    s.print();
    s.printColor();

    Shape s2 = () -> System.out.println("lambda drawing");
    s2.draw();
    s2.print();
  }
}
