package com.basic.functionalInterface;

@FunctionalInterface
public interface Shape  extends Color {
  void draw();
  default void  print(){
    System.out.println("Shape is printed.");
  }
}
