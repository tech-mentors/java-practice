package com.basic.functionalInterface;

public class Rectangle implements  Shape {
  @Override public void draw() {
    System.out.println("Drawing rectangle.");
  }

  @Override public void print() {
    System.out.println("Rectangle Printed.");
  }
}
