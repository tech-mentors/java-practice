package com.basic.mail;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

public class ReadInboundEmailService {
  private static final Logger logger = Logger.getLogger(ReadInboundEmailService.class.getName());
  private static final String PROTOCOL = "imaps";
  private static final String INBOX = "INBOX";
  private static final String PROPERTIES_FILE = "mail.properties";
  private static final String PORT_KEY = "mail.imap.port";
  private static final String HOST_NAME_KEY = "mail.imap.host";
  private static final String USER_NAME_KEY = "email.userName";
  private static final String PASSWORD_KEY = "email.password";

  public static void main(String[] args) {
    ReadInboundEmailService obj = new ReadInboundEmailService();
    Properties properties = obj.getMailProperties();
    logger.info("...........connecting to mail server...............");
    Map<String, MailMessage> mailMessages =
        obj.readMailAndReturnMailObject(properties);
    logger.info(mailMessages.values().toString());
    logger.info("...........end mail api...............");
  }

  public Map<String, MailMessage> readMailAndReturnMailObject(Properties mailProperties) {
    return readMailAndReturnMailObject(mailProperties, false, false, 0);
  }

  public Map<String, MailMessage> readMailAndReturnMailObject(
      Properties mailProperties,
      boolean recentlySorted,
      boolean indexRequired,
      int toBeFetchedCount) {
    try {
      //Create POP3 store object and connect with the server
      Store storeObj = Session.getDefaultInstance(mailProperties).getStore(PROTOCOL);
      storeObj.connect(
          mailProperties.getProperty(HOST_NAME_KEY),
          Integer.parseInt(mailProperties.getProperty(PORT_KEY)),
          mailProperties.getProperty(USER_NAME_KEY),
          mailProperties.getProperty(PASSWORD_KEY));
      //Create folder object and open it in read-only mode
      Folder emailFolderObj = storeObj.getFolder(INBOX);
      emailFolderObj.open(Folder.READ_ONLY);
      //Fetch messages from the folder and print in a loop
      AtomicInteger index = new AtomicInteger(0);
      Stream<Message> messageStream =
          Arrays.stream(emailFolderObj.getMessages());
      // if sorting is required;
      if (recentlySorted) {
        messageStream = messageStream.sorted(sortByReceivedDate());
      }
      if (toBeFetchedCount > 0) {
        messageStream = messageStream.limit(toBeFetchedCount);
      }
      //if indexing is required otherwise create map of subject::body.
      Map<String, MailMessage> messageMap =
          messageStream.map(
                  message -> indexRequired ?
                      createMsgObj(message, index.incrementAndGet()) :
                      createMsgObj(message, 0))
              .collect(toMap(MailMessage::getSubject, identity()));
      //logger.info(messageList.toString());
      //Now close all the objects
      emailFolderObj.close(false);
      storeObj.close();
      return messageMap;
    } catch (Exception exp) {
      logger.info(exp.toString());
      return new HashMap<>(0);
    }
  }

  private Comparator<Message> sortByReceivedDate() {
    return (o1, o2) -> {
      try {
        return o1.getReceivedDate().compareTo(o2.getReceivedDate());
      } catch (MessagingException e) {
        e.printStackTrace();
      }
      return 0;
    };
  }

  private MailMessage createMsgObj(Message message, int index) {
    try {
      String sender = (message.getFrom()[0]).toString();
      String body = getTextFromMessage(message);
      String subject = message.getSubject();
      return new MailMessage(index, sender, subject, body, message.getReceivedDate());
    } catch (IOException | MessagingException exception) {
      return new MailMessage();
    }
  }

  private Properties getMailProperties() {
    Properties prop = new Properties();
    try {
      logger.info("..........Searching properties file............");
      InputStream inputStream = getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE);
      logger.info("..........Properties found............");
      //load a properties file from class path, inside static method
      prop.load(inputStream);
      logger.info("..........Properties loaded............");
    } catch (IOException ex) {
      logger.info(ex.getMessage());
    }
    return prop;
  }

  private String getTextFromMessage(Message message) throws MessagingException, IOException {
    String result = "";
    if (message.isMimeType("text/plain")) {
      result = message.getContent().toString();
    } else if (message.isMimeType("multipart/*")) {
      MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
      result = getTextFromMimeMultipart(mimeMultipart);
    }
    return result;
  }

  private String getTextFromMimeMultipart(MimeMultipart mimeMultipart)
      throws MessagingException, IOException {
    StringBuilder result = new StringBuilder();
    int count = mimeMultipart.getCount();
    for (int i = 0; i < count; i++) {
      BodyPart bodyPart = mimeMultipart.getBodyPart(i);
      if (bodyPart.isMimeType("text/plain")) {
        result.append("\n").append(bodyPart.getContent());
        break; // without break same text appears twice in my tests
      } else if (bodyPart.isMimeType("text/html")) {
        String html = (String) bodyPart.getContent();
        result.append("\n").append(org.jsoup.Jsoup.parse(html).text());
      } else if (bodyPart.getContent() instanceof MimeMultipart) {
        result.append(getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent()));
      }
    }
    return result.toString();
  }

  private static class MailMessage {
    private final int index;
    private final String subject;
    private final String body;
    private final String sender;
    private final Date receivedDate;

    public MailMessage(int index, String sender, String subject, String body, Date receivedDate) {
      this.index = index;
      this.subject = subject;
      this.body = body;
      this.sender = sender;
      this.receivedDate = receivedDate;
    }

    @Override public String toString() {
      String date = (receivedDate == null) ? "null" : receivedDate.toString();
      return "MailMessage{" +
          "index = " + index +
          ", subject = '" + subject + '\'' +
          ", body = '" + body + '\'' +
          ", sender = '" + sender + '\'' +
          ", receivedDate = '" + date + '\'' +
          '}';
    }

    public MailMessage() {
      this(0, "", "", "", null);
    }

    public Date getReceivedDate() {
      return receivedDate;
    }

    public int getIndex() {
      return index;
    }

    public String getSubject() {
      return subject;
    }

    public String getBody() {
      return body;
    }

    public String getSender() {
      return sender;
    }
  }
}
