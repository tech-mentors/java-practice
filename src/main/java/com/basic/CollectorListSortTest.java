package com.basic;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import java.util.Objects;

class BookResponse {
  private int price;

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public BookResponse(int price) {
    this.price = price;
  }

  @Override public String toString() {
    return "BookResponse{" +
        "price=" + price +
        '}';
  }
}

class Book {

  private int price;

  private String name;

  public Book(int price, String name) {
    this.price = price;
    this.name = name;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Book book = (Book) o;
    return price == book.price &&
        Objects.equals(name, book.name);
  }

  @Override public int hashCode() {
    return Objects.hash(price, name);
  }

  @Override public String toString() {
    return "Book{" +
        "price=" + price +
        ", name='" + name + '\'' +
        '}';
  }
}

public class CollectorListSortTest {
  public static void main(String[] args) {
    List<Book> books = new ArrayList<>();
    Book b1 = new Book(10, "java");
    Book b2 = new Book(11, "c");
    Book b3 = new Book(12, "c++");
    Book b4 = new Book(13, "python");
    Book b5 = new Book(14, "math");

    books.add(b1);
    books.add(b2);
    books.add(b3);
    books.add(b4);
    books.add(b5);
    //print
    List<BookResponse> bookResponses =
        books.stream().map(CollectorListSortTest::createResponse).collect(Collectors.toList());

    System.out.println(bookResponses);

    List<Book> sortedBooks = books.stream().sorted(Comparator.comparing(Book::getPrice)).collect(
        Collectors.toList());

    List<BookResponse> bookResponsesSorted = sortedBooks.stream()
        .map(CollectorListSortTest::createResponse)
        .collect(Collectors.toList());

    System.out.println(bookResponsesSorted);
  }

  private static BookResponse createResponse(Book book) {
    return new BookResponse(book.getPrice());
  }
}
