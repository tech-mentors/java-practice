package com.basic;

import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;

public class IntAndStringListSortTest {
  /**
   * error comes when compare method called over integer class and string object.
   *
   * */
  public static void main(String[] args) {
    TreeSet l = new TreeSet();
    l.add("a");
    l.add("A");
    l.add("10");
    l.add(10);
    l.add(100);
    System.out.println("Before sort l = " + l);

    System.out.println("After sort l = " + l);
  }
}
