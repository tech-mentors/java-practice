package com.basic.arrays;

import java.util.*;
import java.util.stream.Collectors;

public class CountPairOfGivenSumInArrayTest {
  public static void main(String[] args) {
    int[] arr = new int[] {1, 5, 7, -1, 5};
    int sum = 6;
    System.out.println(
        "Count of pairs is " + getPairsCount(arr, sum) + " ," + getPairsCount1(arr, sum));
  }

  private static int getPairsCount(int[] arr, int sum) {
    int count = 0;
    int n = arr.length;
    for (int i = 0; i < n - 1; i++) {
      for (int j = i + 1; j < n; j++) {
        if ((sum - arr[i]) == arr[j]) {
          count++;
        }
      }
    }
    return count;
  }

  private static int getPairsCount1(int[] arr, int sum) {
    Map<Integer, Integer> map =
        Arrays.stream(arr).boxed().collect(Collectors.toMap(k -> k, v -> 1, Integer::sum,
            LinkedHashMap::new));
    int twice_count = 0;
    // Store counts of all elements in map hm
    for (int j : arr) {
      if (map.containsKey(sum - j)) {
        twice_count += map.get(sum - j);
      }
    }
    // return the half of twice_count
    return twice_count / 2;
  }
}
