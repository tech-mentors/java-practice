package com.basic.arrays;

import java.util.Arrays;

/**
 * @author sanjeev
 * @apiNote https://www.geeksforgeeks.org/largest-sum-contiguous-subarray/
 */

public class FindLargestSumContiguousSubArrayTest {

    public static void main(String[] args) {
        int[] arr = {-2, -3, 4, -1, -2, 1, 5, -3};
        System.out.println("arr = " + Arrays.toString(arr));
        int resultSum = findLargestSumContiguousSubArray(arr);
        System.out.println("Sum of largest contiguous subArray is:: = " + resultSum);
    }

    private static int findLargestSumContiguousSubArray(int[] arr) {
        int max_so_far = Integer.MIN_VALUE;
        int max_ending_here = 0;
        for (int j : arr) {
            max_ending_here += j;
            if (max_so_far < max_ending_here)
                max_so_far = max_ending_here;
            if (max_ending_here < 0)
                max_ending_here = 0;
        }
        return max_so_far;
    }

}
