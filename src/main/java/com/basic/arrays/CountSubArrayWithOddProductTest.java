package com.basic.arrays;

public class CountSubArrayWithOddProductTest {
  // Driver Code
  public static void main(String[] args) {
    int[] arr = {12, 15, 7, 3, 25, 6, 2, 1, 1, 7};
    int n = arr.length;
    // Function call
    System.out.println(countSubArrayWithOddProduct(arr, n));
  }

  // Function that returns the count of
  // sub-arrays with odd product
  private static int countSubArrayWithOddProduct(int[] A,
      int N) {
    // Initialize the count variable
    int count = 0;

    // Initialize variable to store the
    // last index with even number
    int last = -1;

    // Initialize variable to store
    // count of continuous odd numbers
    int K;

    // Loop through the array
    for (int i = 0; i < N; i++) {

      // Check if the number
      // is even or not
      if (A[i] % 2 == 0) {

        // Calculate count of continuous
        // odd numbers
        K = (i - last - 1);

        // Increase the count of sub-arrays
        // with odd product
        count += (K * (K + 1) / 2);

        // Store the index of last
        // even number
        last = i;
      }
    }

    // N considered as index of
    // even number
    K = (N - last - 1);
    count += (K * (K + 1) / 2);
    return count;
  }
}
