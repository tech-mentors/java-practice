package com.basic.arrays;

import java.util.Arrays;
import java.util.HashMap;

public class PerfectCubeCountSubArrayTest {
  private final static int MAX = (int) (1e5);

  public static void main(String[] args) {
    int[] arr = {1, 8, 4, 2};
    int N = arr.length;
    System.out.println("count of perfect cube by 1st method = " + countSubArrays(arr, N));
    System.out.println("count of perfect cube by 2nd method = " + countSubArrays2(arr, N));
  }

  // Function to count sub arrays
  // whose product is a perfect cube
  private static int countSubArrays(int[] a, int n) {
    // Store the required result
    int ans = 0;
    // Traverse all the sub arrays
    for (int i = 0; i < n; i++) {
      int prod = 1;
      for (int j = i; j < n; j++) {
        prod *= a[j];
        // If product of the current
        // sub array is a perfect cube
        // Increment count
        if (perfectCube(prod)) {
          ans++;
        }
      }
    }
    // Print the result
    return ans;
  }

  // Function to count the number of
  // sub  arrays whose product is a perfect cube
  private static int countSubArrays2(int[] arr, int n) {

    // Store the required result
    int ans = 0;
    // Stores the prime
    // factors modulo 3
    int[] v = new int[MAX];
    // Stores the occurrences
    // of the prime factors
    HashMap<Key, Integer> mp = new HashMap<>();
    mp.put(new Key(v), 1);
    // Traverse the array, arr[]
    for (int i : arr) {
      // Store the prime factors
      // and update the vector v
      primeFactors(v, i);
      Key vv = new Key(v);
      int value = mp.getOrDefault(vv, 0);
      // Update the answer
      ans += value;
      // Increment current state
      // of the prime factors by 1
      mp.put(vv, value + 1);
    }
    return ans;
  }

  // Function to store the prime
  // factorization of a number
  private static void primeFactors(int[] v, int n) {
    for (int i = 2; i * i <= n; i++) {
      // If N is divisible by i
      while (n % i == 0) {
        // Increment v[i] by 1 and
        // calculate it modulo by 3
        v[i]++;
        v[i] %= 3;
        // Divide the number by i
        n /= i;
      }
    }

    // If the number is not equal to 1
    if (n != 1) {
      // Increment v[n] by 1
      v[n]++;
      // Calculate it modulo 3
      v[n] %= 3;
    }
  }

  // Function to check if a number
  // is perfect cube or not
  private static boolean perfectCube(int n) {
    int cube_root;
    // Find the cube_root
    cube_root = (int) Math.round(Math.pow(n, 1.0 / 3.0));
    // If cube of cube_root is
    // same as n, then return true
    return cube_root * cube_root * cube_root == n;
    // Otherwise
  }

  // To store the arr as a Key in map
  private static class Key {
    private final int[] arr;

    private Key(int[] arr) {
      this.arr = arr;
    }

    @Override public int hashCode() {
      return 31 + Arrays.hashCode(arr);
    }

    @Override public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null || (getClass() != obj.getClass())) {
        return false;
      }
      Key other = (Key) obj;
      return Arrays.equals(arr, other.arr);
    }
  }


}
