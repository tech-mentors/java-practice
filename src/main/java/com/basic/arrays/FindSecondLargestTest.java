package com.basic.arrays;

import com.basic.utils.RandomUtils;
import java.util.Arrays;

public class FindSecondLargestTest {
  public static void main(String[] args) {
    int[] data = new int[10];

    for (int i = 0; i < 10; i++) {
      data[i] = RandomUtils.random();
    }
    System.out.println("Arrays.toString(data = " + Arrays.toString(data));
    System.out.println("2nd highest data = " + findSecondLargest(data));
  }

  public static int findSecondLargest(int[] data) {
    if (data.length < 2) {
      throw new IllegalArgumentException("can't be 2 length");
    }
    int max = data[0];
    int secondMax = data[0];
    int n = data.length;
    for (int i = 1; i < n; i++) {
      if (max < data[i]) {
        secondMax = max;
        max = data[i];
      } else if (data[i] < max && secondMax < data[i]) {
        secondMax = data[i];
      }
    }

    return secondMax;
  }
}

