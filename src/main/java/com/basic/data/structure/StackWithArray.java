package com.basic.data.structure;

import static com.basic.utils.PrintUtils.printlnStr;

public final class StackWithArray {
  private static final int[] dataArray = new int[10];
  private int top = -1;

  public int pop() {
    if (isEmpty()) {
      printlnStr("UnderFlowFlow Condition.");
      return Integer.MIN_VALUE;
    }
    return dataArray[top--];
  }

  public int peek() {
    if (isEmpty()) {
      printlnStr("UnderFlowFlow Condition.");
      return Integer.MIN_VALUE;
    }
    return dataArray[top];
  }

  public boolean isEmpty() {
    return top < 0;
  }

  public boolean push(int data) {
    if (top >= dataArray.length) {
      printlnStr("OverFlow Condition.");
      return false;
    } else {
      dataArray[++top] = data;
      return true;
    }
  }
}
