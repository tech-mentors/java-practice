package com.basic.data.structure;
// Java Program to Implement StackWithArrayList in Java Using Array and
// Generics

import java.util.*;

// user defined class for generic stack
public class StackWithArrayList<T> {

  // Empty array list
  private final ArrayList<T> DATA_ARRAY;

  // Default value of top variable when stack is empty
  int top = -1;

  // Variable to store size of array
  int size;

  // Constructor of this class
  // To initialize stack
  public StackWithArrayList(int size) {
    // Storing the value of size into global variable
    this.size = size;

    // Creating array of Size = size
    this.DATA_ARRAY = new ArrayList<>(size);
  }

  // Method 1
  // To push generic element into stack
  public T push(T X) {
    // Checking if array is full
    if (top + 1 == size) {

      // Display message when array is full
      System.out.println("StackWithArrayList Overflow");
    } else {

      // Increment top to go to next position
      top = top + 1;

      // Over-writing existing element
      if (DATA_ARRAY.size() > top) {
        DATA_ARRAY.set(top, X);
      } else

      // Creating new element
      {
        DATA_ARRAY.add(X);
      }
    }
    return X;
  }

  public T peek() {
    return top();
  }

  // Method 2
  // To return topmost element of stack
  public T top() {
    // If stack is empty
    if (top == -1) {

      // Display message when there are no elements in
      // the stack
      System.out.println("StackWithArrayList Underflow");

      return null;
    }

    // else elements are present so
    // return the topmost element
    else {
      return DATA_ARRAY.get(top);
    }
  }

  // Method 3
  // To delete last element of stack
  public T pop() {
    T topE = null;
    // If stack is empty
    if (top == -1) {

      // Display message when there are no elements in
      // the stack
      System.out.println("StackWithArrayList Underflow");
    } else

    // Delete the last element
    // by decrementing the top
    {
      //store top element then decrease top
      topE = peek();
      top--;
    }
    return topE;
  }

  // Method 4
  // To check if stack is empty or not
  public boolean isEmpty() {
    return top == -1;
  }

  // Method 5
  // To print the stack
  // @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < top; i++) {
      builder.append(DATA_ARRAY.get(i)).append("->");
    }
    builder.append(DATA_ARRAY.get(top));
    return builder.toString();
  }

  public int size() {
    return size;
  }
}


