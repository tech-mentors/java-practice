package com.basic.data.structure;

public class BinaryTree {
  // Root of Binary Tree
  public Node root;

  public BinaryTree() {
    root = null;
  }

  /* Given a binary tree, print its nodes according to the
 "bottom-up" postorder traversal. */
  private void printPostorder(Node node) {
    if (node == null) {
      return;
    }

    // first recur on left subtree
    printPostorder(node.left);

    // then recur on right subtree
    printPostorder(node.right);

    // now deal with the node
    System.out.print(node.key + " ");
  }

  /* Given a binary tree, print its nodes in inorder*/
  private void printInorder(Node node) {
    if (node == null) {
      return;
    }

    /* first recur on left child */
    printInorder(node.left);

    /* then print the data of node */
    System.out.print(node.key + " ");

    /* now recur on right child */
    printInorder(node.right);
  }

  /* Given a binary tree, print its nodes in preorder*/
  private void printPreorder(Node node) {
    if (node == null) {
      return;
    }

    /* first print data of node */
    System.out.print(node.key + " ");

    /* then recur on left subtree */
    printPreorder(node.left);

    /* now recur on right subtree */
    printPreorder(node.right);
  }

  // Wrappers over above recursive functions
  public void printPostorder() {
    printPostorder(root);
  }

  public void printInorder() {
    printInorder(root);
  }

  public void printPreorder() {
    printPreorder(root);
  }

  public static class Node {
   public int key;
   public Node left, right;

    public Node(int item) {
      key = item;
      left = right = null;
    }
  }
}