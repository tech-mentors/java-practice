package com.basic.data.structure;

/**
 * used for creating linked list.
 * This is linear data structure.
 * contains value and reference of other node.
 */

public class LinkedList {
  //point to header for do traversing
  public Node head;

  /*
   * static inner class to handle node base data structure pattern.
   * @data contains actual value.
   * @next contains reference of other node.
   * if next is null then list is ended.
   */
  public static class Node {
    public int data;
    public Node next;

    // Constructor
    public Node(int d) {
      data = d;
      next = null;
    }
  }

  public boolean isEmpty() {
    return head == null;
  }
}
