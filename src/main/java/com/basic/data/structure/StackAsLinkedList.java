package com.basic.data.structure;

// Java Code for Linked List Implementation

public class StackAsLinkedList {

  LinkedList.Node root;
  public boolean isEmpty()
  {
    return root == null;
  }

  public void push(int data)
  {
    LinkedList.Node newNode = new LinkedList.Node(data);

    if (root == null) {
      root = newNode;
    }
    else {
      LinkedList.Node temp = root;
      root = newNode;
      newNode.next = temp;
    }
    System.out.println(data + " pushed to stack");
  }

  public int pop()
  {
    int popped = Integer.MIN_VALUE;
    if (root == null) {
      System.out.println("Stack is Empty");
    }
    else {
      popped = root.data;
      root = root.next;
    }
    return popped;
  }

  public int peek()
  {
    if (root == null) {
      System.out.println("Stack is empty");
      return Integer.MIN_VALUE;
    }
    else {
      return root.data;
    }
  }

  
}

