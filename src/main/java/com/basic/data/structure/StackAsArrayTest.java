package com.basic.data.structure;

import com.basic.data.structure.StackWithArray;

import static com.basic.utils.PrintUtils.printlnStr;
import static com.basic.utils.RandomUtils.random;

// Java Code for Array Implementation



public class StackAsArrayTest {
  public static void main(String[] args) {
    printlnStr("Implementing StackWithArray using Array....");
    StackWithArray stack = new StackWithArray();
    printlnStr("Checking underflow condition before push....");
    printlnStr("Is StackWithArray empty::" + stack.isEmpty());
    int p = stack.peek();
    printlnStr("data in case of underflow::" + p);
    printlnStr("Pushing random underflow condition before push....");
    boolean result = stack.push(random());
    printlnStr("One Element  is pushed in stack successfully::" + result);
    printlnStr("Is StackWithArray empty::" + stack.isEmpty());
    int p1 = stack.peek();
    printlnStr("Data after one push::" + p1);
    printlnStr("Pop element after push....");
    int p2 = stack.pop();
    printlnStr("Data after one pop::" + p2);
    printlnStr("Is StackWithArray empty::" + stack.isEmpty());
    int counter = 1;
    do {
      int data = random();
      boolean pushed = stack.push(data);
      printlnStr("One Element '" + data + "'  is pushed in stack successfully::" + pushed);
      ++counter;
    } while (counter < 10);
    printlnStr("Printing stack.");

    do {
      int p3 = stack.pop();
      printlnStr("Data after one pop::" + p3);
    } while (stack.peek() >= 0);
  }
}
