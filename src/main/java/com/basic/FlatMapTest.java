package com.basic;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class FlatMapTest {
    public static void main(String[] args)
    {
        List<Object> list1 = Arrays.asList(1,2,3);
        List<Object> list2 = Arrays.asList("s3","53","6");
        List<Object> list3 = Arrays.asList(7,8,9);

        List<List<Object>> listOfLists = Arrays.asList(list1, list2, list3);

        List<Object> listOfAllIntegers = listOfLists.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        System.out.println(listOfAllIntegers);
    }
}


