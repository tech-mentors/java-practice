package com.basic.utils;

public class PrintUtils {

  private PrintUtils() {
    throw new IllegalStateException("Can'nt initialized.");
  }

  public static void printStr(final String aString) {
    System.out.print(aString);
  }

  public static void printStr(final int aInt) {
    printStr(String.valueOf(aInt));
  }

  public static void printlnStr(final String aString) {
    System.out.println(aString);
  }
}
