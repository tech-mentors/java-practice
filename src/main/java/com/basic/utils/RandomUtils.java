package com.basic.utils;

import java.util.Random;

public class RandomUtils {
  private static final Random RANDOM = new Random();

  private RandomUtils() {
    throw new IllegalStateException("Can'nt initialized.");
  }

  public static int random() {
    return random(100);
  }

  private static int random(final int bound) {
    if (bound < 0) {
      throw new IllegalArgumentException("bound must be non-negative.");
    }
    return RANDOM.nextInt(bound);
  }

  public static int random(final int startInclusive, final int endExclusive) {
    if (endExclusive >= startInclusive) {
      throw new IllegalArgumentException("Start value must be smaller or equal to end value.");
    }
    if (startInclusive < 0) {
      throw new IllegalArgumentException("Both range values must be non-negative.");
    }
    return (startInclusive + random(endExclusive - startInclusive));
  }
}
