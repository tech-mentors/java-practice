package com.basic.utils;

import com.basic.data.structure.LinkedList;
import java.util.Random;
import java.util.Scanner;

import static com.basic.utils.PrintUtils.printStr;
import static com.basic.utils.PrintUtils.printlnStr;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class LinkListUtil {

  private static final Random RANDOM = new Random();
  private static final Scanner SCANNER = new Scanner(System.in);

  private LinkListUtil() {
    throw new IllegalStateException("Can'nt initialized.");
  }

  /**
   * @param list in which value will inserted
   * @param data actual integer type value which is inserted in list.
   * @return return list after insertion.
   * check is list is null then initialized that first.
   */
  public static LinkedList nullSafeInsertInList(LinkedList list, final int data) {
    //check null first
    if (isNull(list)) {
      list = new LinkedList();
    }
    // Create a new node with given data
    LinkedList.Node new_node = new LinkedList.Node(data);
    new_node.next = null;
    // If the Linked List is empty,
    // then make the new node as head
    if (isNull(list.head)) {
      list.head = new_node;
    } else {
      // Else traverse till the last node
      // and insert the new_node there
      LinkedList.Node last = list.head;
      while (nonNull(last.next)) {
        last = last.next;
      }
      // Insert the new_node at last node
      last.next = new_node;
    }
    // Return the list by head
    return list;
  }

  /**
   * fill linked list with random no 1-100;
   */
  public static LinkedList nullSafeFillListWithRandom(LinkedList list, int size) {
    while (size > 0) {
      list = nullSafeInsertInList(list, random());
      size--;
    }
    return list;
  }

  public static void fillLinkedListFromConsole(LinkedList linkedList) {
    int c;
    do {
      printlnStr("enter value::");
      int value = SCANNER.nextInt();
      linkedList = nullSafeInsertInList(linkedList, value);
      printlnStr("Do you add more in list");
      c = SCANNER.nextInt();
    } while (c == 0);
  }

  private static int random() {
    return RANDOM.nextInt(100);
  }

  /**
   * @param desiredMsg msg which will print before list
   * @param linkedList list which be printed
   * print like
   * if null then linked list is null.
   * if empty then linked list is empty.
   * if not empty then [1,2,3]
   */
  public static void nullSafePrintList(LinkedList linkedList, final String desiredMsg) {
    if (nonNull(linkedList) && nonNull(linkedList.head)) {
      //print message first.
      printlnStr(desiredMsg);
      printStr("     [");
      LinkedList.Node last = linkedList.head;
      do {
        printStr(last.data);
        if (nonNull(last.next)) {
          printStr(",");
        }
        last = last.next;
      } while (nonNull(last));
      printlnStr("]");
    } else if (nonNull(linkedList)) {
      printlnStr("linked list is empty.");
    } else {
      printlnStr("linked list is null.");
    }
  }
}
