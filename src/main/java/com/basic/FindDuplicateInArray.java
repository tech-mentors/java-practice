package com.basic;

// Java code to find
// duplicates in O(n) time

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class FindDuplicateInArray {
  // Function to print duplicates
  private Integer[] printRepeating(int[] arr, int size) {
    int i;
   Set<Integer> result = new HashSet<>(size/2);
    System.out.println("The repeating elements are : ");
    int k = 0;
    for (i = 0; i < size; i++) {
      int j = Math.abs(arr[i]);
      if (arr[j] >= 0) {
        arr[j] = -arr[j];
      } else {
        result.add(j);
        k++;
      }
    }
    if (result.size()== 0) {
      result.add(-1);
    }
    return result.toArray(new Integer[result.size()]);
  }

  // Driver code
  public static void main(String[] args) {
    FindDuplicateInArray duplicate = new FindDuplicateInArray();
    int[] arr = {1, 2, 3, 1, 3, 6, 6};
    int arr_size = arr.length;

    Integer[] result = duplicate.printRepeating(arr, arr_size);
    System.out.println("result = " + Arrays.toString(result));
  }
}

