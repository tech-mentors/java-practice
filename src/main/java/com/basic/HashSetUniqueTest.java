package com.basic;

import com.basic.model.Vehicle;
import java.util.HashSet;
import java.util.Set;

public class HashSetUniqueTest {
  public static void main(String[] args) {
    Set<Vehicle> vehicles= new HashSet<>() ;
    System.out.println("vehicles = " + vehicles);
    //call before overriding equals and hash code  then with overriding
    for (int i=0;i<100;i++){
      vehicles.add(new Vehicle("v1",1));
    }
    System.out.println("vehicles = " + vehicles);
  }
}
