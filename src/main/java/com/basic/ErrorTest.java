package com.basic;

public class ErrorTest {

  public static void main(String[] args) {
    try {
      badMethod();
      System.out.println("A is called.");
    } catch (Exception e) {
      System.out.println("b is called.");
    } finally {
      System.out.println("c is called.");
    }
    System.out.println("d is called.");
  }

  private static void badMethod() {
    throw new Error();
  }
}
