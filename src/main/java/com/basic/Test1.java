package com.basic;

import com.basic.utils.RandomUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//1.> list o fileter 10<
public class Test1 {

  public static void main(String[] args) {
    List<Integer> list = new ArrayList<>(10);
    list.add(10);
    list.add(3);
    list.add(39);
    list.add(2);
    list.add(45);
    list.add(6);
    System.out.println("Map = " + randomKeyMapper(list));
  }

  public static Map<Integer, Integer> randomKeyMapper(List<Integer> lists) {
    return lists.stream()
        .filter(k -> k>=10)
        .collect(Collectors.toMap(k -> RandomUtils.random(), v -> v, (k, v) -> v, HashMap::new));
  }
}
