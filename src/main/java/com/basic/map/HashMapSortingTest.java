package com.basic.map;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class HashMapSortingTest {
  public static void main(String[] args) {
    Map<String, Integer> map = new HashMap<>();
    // putting values in the Map
    map.put("Danish", 40);
    map.put("Rajeev", 80);
    map.put("Abhishek", 90);
    map.put("Anushka", 80);
    map.put("Amit", 75);

    //map.forEach((s, v) -> {
    //  System.out.println("s = " + s);
    //  System.out.println("v = " + v);
    //});

    // print the sorted hashmap
    System.out.println("Before sorting map.. ");
    for (Map.Entry<String, Integer> en :
        map.entrySet()) {
      System.out.println("Key = " + en.getKey()
          + ", Value = "
          + en.getValue());
    }
    //map.forEach((s, integer) -> {
    //      System.out.println("s = " + s);
    //      System.out.println("int = " + integer);
    //    }
    //);

    // Calling the function to sort by Key
    Map<String, Integer> hm1 = sortByKey(map);
    System.out.println("\nAfter sorting map.. ");
    // print the sorted hashmap
    for (Map.Entry<String, Integer> en :
        hm1.entrySet()) {
      System.out.println("Key = " + en.getKey()
          + ", Value = "
          + en.getValue());
    }

    // Calling the function to sort by Key
    Map<String, Integer> hm2 = sortByKey2(map);
    System.out.println("\nAfter sorting map using treemap.. ");
    // print the sorted hashmap
    for (Map.Entry<String, Integer> en :
        hm2.entrySet()) {
      System.out.println("Key = " + en.getKey()
          + ", Value = "
          + en.getValue());
    }
  }

  private static Map<String, Integer> sortByKey(Map<String, Integer> map) {
    Set<String> sortedSet = map.keySet().stream().sorted().collect(
        Collectors.toCollection(LinkedHashSet::new));
    Map<String, Integer> sortedMap = new LinkedHashMap<>();
    for (String s : sortedSet) {
      sortedMap.put(s, map.get(s));
    }
    //Comparator<Map.Entry<String,Integer>> comparator= ;
    //
    //
    // sortedMap.entrySet().stream().sorted(comparator).collect(
    //    Collectors.toList());

     return null;
  }

  private static Map<String, Integer> sortByKey2(Map<String, Integer> map) {
    TreeMap<String, Integer> treeMap = new TreeMap<>(map);
    return new LinkedHashMap<>(treeMap);
  }
}


