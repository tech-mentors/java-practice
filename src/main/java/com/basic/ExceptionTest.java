package com.basic;

import java.io.IOException;

public class ExceptionTest {
  static {
    String s1 = "Sanjeev";
    String s2 = "kumar";
    System.out.println("true = " + (s1 = s2));
  }

  public static void main(String[] args) throws Exception {
    System system = null;
    System.out.println("args = " + returnCheck(null));
    System.out.println(system);
  }

  public static int returnCheck() throws Exception {

    float[] a = new float[1];
    Object o = a;
    a[1] = 30 / 0;
    return 0;
  }

  public static int returnCheck(Long l) throws IOException {

    float[] a = new float[1];
    Object o = a;
    a[0] = 30 / 10;
    return 0;
  }
}
