package com.basic.linkedList;

import com.basic.data.structure.LinkedList;
import java.util.Scanner;

import static com.basic.utils.LinkListUtil.nullSafeInsertInList;
import static com.basic.utils.LinkListUtil.nullSafePrintList;
import static com.basic.utils.PrintUtils.printlnStr;

public class LinkedListTraversal {
  /* Driver program to test above functions */
  public static void main(String[] args) {
    LinkedList linkedList = new LinkedList();
    fillLinkedList(linkedList);
    traverseList(linkedList);
  }

  private static void fillLinkedList(LinkedList linkedList) {
    Scanner scanner = new Scanner(System.in);
    int c;
    do {
      printlnStr("enter value::");
      int value = scanner.nextInt();
      linkedList = nullSafeInsertInList(linkedList, value);
      printlnStr("Do you add more in list");
      c = scanner.nextInt();
    } while (c == 0);
  }

  private static void traverseList(LinkedList linkedList) {
    //print list
    nullSafePrintList(linkedList, "Your linked list is ");
  }
}
