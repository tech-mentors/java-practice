package com.basic.linkedList;

import com.basic.data.structure.LinkedList;

import static com.basic.utils.LinkListUtil.nullSafeFillListWithRandom;
import static com.basic.utils.LinkListUtil.nullSafePrintList;
import static com.basic.utils.PrintUtils.printlnStr;

public class LinkedListReverse {

  /* Driver program to test above functions */
  public static void main(String[] args) {
    LinkedList linkedList = new LinkedList();
    //fillLinkedList(linkedList);
    linkedList = nullSafeFillListWithRandom(linkedList, 15);
    nullSafePrintList(linkedList, "Before, reverse,Your linked list is ");
    reverseList(linkedList);
  }

  private static void reverseList(LinkedList linkedList) {
    LinkedList.Node previous = null, next, current = linkedList.head;
    while (current != null) {
      next = current.next;
      current.next = previous;
      previous = current;
      current = next;
    }
    linkedList.head = previous;
    printlnStr("");
    nullSafePrintList(linkedList, "After, reverse,Your linked list is ");
  }
}
