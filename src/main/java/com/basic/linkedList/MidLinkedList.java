package com.basic.linkedList;

import com.basic.data.structure.LinkedList;

import static com.basic.utils.LinkListUtil.nullSafeFillListWithRandom;
import static com.basic.utils.LinkListUtil.nullSafePrintList;
import static com.basic.utils.PrintUtils.printlnStr;

public class MidLinkedList {
  /* Driver program to test above functions */
  public static void main(String[] args) {
    LinkedList linkedList = new LinkedList();
    linkedList = nullSafeFillListWithRandom(linkedList, 4);
    traverseList(linkedList);
    printMiddle1(linkedList);
    printMiddle2(linkedList);
  }

  private static void traverseList(LinkedList linkedList) {
    //print list
    nullSafePrintList(linkedList, "Your linked list is ");
  }

  /* Function to print middle of linked list */
  private static void printMiddle1(LinkedList linkedList) {
    LinkedList.Node slow_ptr = linkedList.head;
    LinkedList.Node fast_ptr = linkedList.head;
    if (linkedList.head != null) {
      while (fast_ptr != null && fast_ptr.next != null) {
        fast_ptr = fast_ptr.next.next;
        slow_ptr = slow_ptr.next;
      }
      printlnStr("Sol 1:: The middle element is [" +
          slow_ptr.data + "] ");
    }
  }

  //solution 2
  // Function to get the middle of
  // the linked list
  private static void printMiddle2(LinkedList linkedList) {
    int count = 0;
    LinkedList.Node mid = linkedList.head;

    while (linkedList.head != null) {

      // Update mid, when 'count'
      // is odd number
      if ((count % 2) == 1) {
        mid = mid.next;
      }

      ++count;
      linkedList.head = linkedList.head.next;
    }

    // If empty list is provided
    if (mid != null) {
      printlnStr("Sol 2:: The middle element is [" +
          mid.data + "]");
    }
  }
}
