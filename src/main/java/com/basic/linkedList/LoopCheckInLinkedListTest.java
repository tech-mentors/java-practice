package com.basic.linkedList;

import com.basic.data.structure.LinkedList;

import static com.basic.utils.LinkListUtil.nullSafeFillListWithRandom;
import static com.basic.utils.LinkListUtil.nullSafePrintList;
import static com.basic.utils.PrintUtils.printlnStr;

// Java program to detect loop in a linked list
public class LoopCheckInLinkedListTest {

  private static void detectLoop(LinkedList linkedList) {
    LinkedList.Node slow_p = linkedList.head, fast_p = linkedList.head;
    int flag = 0;
    while (slow_p != null && fast_p != null && fast_p.next != null) {
      slow_p = slow_p.next;
      fast_p = fast_p.next.next;
      if (slow_p == fast_p) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      printlnStr("Loop found");
    } else {
      printlnStr("Loop not found");
    }
  }

  /* Driver program to test above functions */
  public static void main(String[] args) {
    LinkedList linkedList = new LinkedList();
    linkedList = nullSafeFillListWithRandom(linkedList, 10);
    nullSafePrintList(linkedList, "Linked list is ::");
    /*Create loop for testing */
    //linkedList.head.next.next.next.next = linkedList.head;
    detectLoop(linkedList);
  }
}

