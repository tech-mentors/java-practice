package com.basic.exceptions;

// Java Program to Illustrate Exception Handling
// with Method Overriding
// Where SuperClass declares an exception and
// SubClass declares a child exception
// of the SuperClass declared Exception

// Importing required classes

import java.io.*;
import java.util.Collections;

class SuperClass {

  // SuperClass declares an exception
  void method() throws EOFException {
    System.out.println("SuperClass");
  }
}

// SuperClass inherited by the SubClass
class SubClass extends SuperClass {

  // SubClass declaring a child exception
  // of RuntimeException
  void method() throws EOFException {
    // ArithmeticException is a child exception
    // of the RuntimeException
    // So the compiler won't give an error
    System.out.println("SubClass");
  }
}

public class OverRideExceptionTest {

  // Driver code
  public static void main(String[] args) throws IOException {
    SuperClass s = new SubClass();
    s.method();
  }
}
