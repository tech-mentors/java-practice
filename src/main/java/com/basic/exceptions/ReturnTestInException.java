package com.basic.exceptions;

public class ReturnTestInException {
  static final public void main(String[] args) {
    System.out.println("args = " + callMe(args));
  }

  private static String callMe(String[] args) {
    try {
      int i=12/0;

      return args.length + "";
    } catch (Exception e) {
     // System.exit(1);
      return "catch";
    } finally {
      System.out.println("i m final");
     // return "final";
    }
  }
}
