package com.basic.design.patterns;

import java.util.ArrayList;

public class ParkingSlotDesignExampleTest {
  public static void main(String[] args) {

  }
}

// Vehicle and its inherited classes.
enum VehicleSize {Motorcycle, Compact, Large}

abstract class Vehicle {
  protected ArrayList<ParkingSpot> parkingSpots =
      new ArrayList<>();
  protected String licensePlate;
  protected int spotsNeeded;
  protected VehicleSize size;

  public int getSpotsNeeded() {
    return spotsNeeded;
  }

  public VehicleSize getSize() {
    return size;
  }

  /* Park vehicle in this spot (among others,
    potentially) */
  public void parkinSpot(ParkingSpot s) {
    parkingSpots.add(s);
  }

  /* Remove vehicle from spot, and notify spot
    that it's gone */
  public void clearSpots() {
    System.out.println("parkingSpots is clear. ");
  }

  /* Checks if the spot is big enough for the
    vehicle (and is available).
    This * compares the SIZE only.It does not
    check if it has enough spots. */
  public abstract boolean canFitInSpot(ParkingSpot spot);
}

class Bus extends Vehicle {
  public Bus() {
    spotsNeeded = 5;
    size = VehicleSize.Large;
  }

  /* Checks if the spot is a Large. Doesn't check
  num of spots */
  public boolean canFitInSpot(ParkingSpot spot) {
    return true;
  }
}

class Level {
  private int level;

  public int getLevel() {
    return level;
  }

  public void setLevel(int level) {
    this.level = level;
  }
}

class Car extends Vehicle {
  public Car() {
    spotsNeeded = 1;
    size = VehicleSize.Compact;
  }

  /* Checks if the spot is a Compact or a Large. */
  public boolean canFitInSpot(ParkingSpot spot) {
    return true;
  }
}

class Motorcycle extends Vehicle {
  public Motorcycle() {
    spotsNeeded = 1;
    size = VehicleSize.Motorcycle;
  }

  public boolean canFitInSpot(ParkingSpot spot) {
    return true;
  }
}

class ParkingSpot {
  private Vehicle vehicle;
  private VehicleSize spotSize;
  private int row;
  private int spotNumber;
  private Level level;

  public ParkingSpot(Level lvl, int r, int n,
      VehicleSize s) {

  }

  public boolean isAvailable() {
    return vehicle == null;
  }

  /* Check if the spot is big enough and is available */
  public boolean canFitVehicle(Vehicle vehicle) {
    return true;
  }

  /* Park vehicle in this spot. */
  public boolean park(Vehicle v) {
    return true;
  }

  public int getRow() {
    return row;
  }

  public int getSpotNumber() {
    return spotNumber;
  }

  /* Remove vehicle from spot, and notify
  level that a new spot is available */
  public void removeVehicle() {
    System.out.println("vehicle removed. ");
  }
}



