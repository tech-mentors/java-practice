package com.basic.design.patterns;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Constructor;

import static com.basic.utils.PrintUtils.printlnStr;

class Singleton implements Cloneable, Serializable {
  public static final Singleton INSTANCE = new Singleton();
  private static int counter = 0;
  private String tag;

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  private Singleton() {
    ++counter;
    this.tag = "first";
    if (INSTANCE != null) {
      throw new IllegalCallerException("can't do.");
    }
    printlnStr("Singleton Object Created. " + counter + " times.");
  }

  //handle clone
  @Override
  protected Object clone() throws CloneNotSupportedException {
    return INSTANCE;
  }

  //called at time readObject Method
  protected Object readResolve() {
    return INSTANCE;
  }
}

//Main class
public class SingletonClassWithProofTest {

  public static void main(String[] str) {
   // using reflection
    Singleton singleton4 = null;
    try {
      Class<Singleton> singleton1Class = (Class<Singleton>) Class.forName(
          "com.basic.design.patterns.Singleton");
      Constructor<Singleton> constructor = singleton1Class.getDeclaredConstructor();
      constructor.setAccessible(true);
      singleton4 = constructor.newInstance();
      singleton4.setTag("three");
      constructor.setAccessible(false);
    } catch (Exception e) {
      System.out.println("reflection not supported");
    }
    //
    Singleton singleton1 = Singleton.INSTANCE;
    //check clone
    Singleton singleton2 = null;
    try {
      singleton2 = (Singleton) singleton1.clone();
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
    }

    //check serialization and deSerialization.
    File file = new File("Singleton.obj");
    try (FileOutputStream fileOutputStream = new FileOutputStream(file);
         ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)
    ) {
      objectOutputStream.writeObject(singleton1);
      printlnStr("singleton1 is serialized.");
    } catch (IOException e) {
      e.printStackTrace();
    }
    //change tag value to check same object
    singleton1.setTag("second");
    Singleton singleton3 = null;
    try (FileInputStream fileInputStream = new FileInputStream(file);
         ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)
    ) {
      singleton3 = (Singleton) objectInputStream.readObject();
      printlnStr("singleton1 is deSerialized.");
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
    }

    if (singleton1 != singleton2 || !singleton1.getTag().equals(singleton2.getTag())) {
      printlnStr("singleton1 != singleton2 is not true.");
    } else if (singleton3 != singleton1 || !singleton1.getTag().equals(singleton3.getTag())) {
      printlnStr("singleton3 != singleton1 is not true.");
    } else if (singleton4 != singleton1 || !singleton1.getTag().equals(singleton4.getTag())) {
      if (singleton4 == null) {
        printlnStr("can't able to create object using reflection.");
      } else {
        printlnStr("singleton4 != singleton1 is not true.");
      }
    } else {
      printlnStr("class is singleton.");
    }
  }
}
