package com.basic.design.patterns;

interface Menus {
  void displayMe();
}

class NonVegMenu implements Menus {
  @Override public void displayMe() {
    System.out.println("This is NonVegMenu menu.");
  }
}

class VegMenu implements Menus {
  @Override public void displayMe() {
    System.out.println("This is VegMenu menu");
  }
}

class Both implements Menus {
  @Override public void displayMe() {
    System.out.println("This is VegMenu and non-VegMenu both menu.");
  }
}

interface Hotel {
  Menus getMenus();
}

class NonVegRestaurant implements Hotel {
  public Menus getMenus() {
    return new NonVegMenu();
  }
}

class VegRestaurant implements Hotel {
  public Menus getMenus() {
    return new VegMenu();
  }
}

class VegNonBothRestaurant implements Hotel {
  public Menus getMenus() {
    return new Both();
  }
}

class HotelKeeper {
  public VegMenu getVegMenu() {
    VegRestaurant v = new VegRestaurant();
    return (VegMenu) v.getMenus();
  }

  public NonVegMenu getNonVegMenu() {
    NonVegRestaurant v = new NonVegRestaurant();
    return (NonVegMenu) v.getMenus();
  }

  public Both getVegNonBothMenu() {
    VegNonBothRestaurant v = new VegNonBothRestaurant();
    return (Both) v.getMenus();
  }
}

public class FacadeDesignPatternTest {
  public static void main(String[] args) {
    HotelKeeper keeper = new HotelKeeper();

    VegMenu v = keeper.getVegMenu();
    NonVegMenu nv = keeper.getNonVegMenu();
    Both b = keeper.getVegNonBothMenu();
    v.displayMe();
    nv.displayMe();
    b.displayMe();
  }
}
