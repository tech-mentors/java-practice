package com.basic.graph;

import com.basic.data.structure.BinaryTree;

public class BinaryTreeTraversingTest {

  /*

   graph is

           1
         /  \
        2    3
       / \
      4  5


 */

  // Driver method
  public static void main(String[] args) {
    BinaryTree tree = new BinaryTree();
    tree.root = new BinaryTree.Node(1);
    tree.root.left = new BinaryTree.Node(2);
    tree.root.right = new BinaryTree.Node(3);
    tree.root.left.left = new BinaryTree.Node(4);
    tree.root.left.right = new BinaryTree.Node(5);

    System.out.println(
        "Preorder traversal of binary tree is ");
    tree.printPreorder();

    System.out.println(
        "\nInorder traversal of binary tree is ");
    tree.printInorder();

    System.out.println(
        "\nPostorder traversal of binary tree is ");
    tree.printPostorder();
  }
}

