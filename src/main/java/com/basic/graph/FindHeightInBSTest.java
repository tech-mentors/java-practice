package com.basic.graph;

import com.basic.data.structure.BinarySearchTree;

// Java program to demonstrate
// insert operation in binary
// search tree
public class FindHeightInBSTest {

  public static boolean searchElementInBst(BinarySearchTree.Node root, int key) {
    if (root != null) {
      return (root.key == key) |
          searchElementInBst(root.left, key) |
          searchElementInBst(root.right, key);
    }
    return false;
  }

  // Driver Code
  public static void main(String[] args) {
    BinarySearchTree tree = new BinarySearchTree();

        /* Let us create following BST
              50
           /     \
          30      70
         /  \    /  \
       20   40  60   80 */
    tree.insert(50);
    tree.insert(30);
    tree.insert(20);
    tree.insert(40);
    tree.insert(70);
    tree.insert(60);
    tree.insert(80);

    // print inorder traversal of the BST
    tree.inorder();
    int key = 30;
    if (searchElementInBst(tree.root, key)) {
      System.out.println("key " + key + " is  found");
    } else {
      System.out.println("key " + key + " is not  found");
    }
  }
}
