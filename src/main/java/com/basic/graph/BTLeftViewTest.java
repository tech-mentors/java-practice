package com.basic.graph;

import com.basic.data.structure.BinaryTree;
import java.util.LinkedList;
import java.util.Queue;

public class BTLeftViewTest {

  // Recursive function to print the left view of a given binary tree
  public static int leftView(BinaryTree.Node root, int level, int last_level) {
    // base case: empty tree
    if (root == null) {
      return last_level;
    }

    // if the current node is the first node of the current level
    if (last_level < level) {
      // print the node's data
      System.out.print(root.key + " ");

      // update the last level to the current level
      last_level = level;
    }

    // recur for the left and right subtree by increasing the level by 1
    last_level = leftView(root.left, level + 1, last_level);
    last_level = leftView(root.right, level + 1, last_level);

    return last_level;
  }

  // Function to print the left view of a given binary tree
  public static void leftView(BinaryTree.Node root) {
    leftView(root, 1, 0);
  }

  // function to print left view of binary tree
  private static void printLeftView(BinaryTree.Node root) {
    if (root == null) {
      return;
    }

    Queue<BinaryTree.Node> queue = new LinkedList<>();
    queue.add(root);

    while (!queue.isEmpty()) {
      // number of nodes at current level
      int n = queue.size();
      //System.out.println("n = " + n);
      // Traverse all nodes of current level
      for (int i = 1; i <= n; i++) {
        BinaryTree.Node temp = queue.poll();
        if (temp == null) continue;
        // Print the left most element at
        // the level
        if (i == 1) {
          System.out.print(temp.key + " ");
        }

        // Add left node to queue
        if (temp.left != null) {
          queue.add(temp.left);
        }

        // Add right node to queue
        if (temp.right != null) {
          queue.add(temp.right);
        }
      }
    }
  }

  // Driver code
  public static void main(String[] args) {
    // construct binary tree as shown in

    /*
     *                         10
     *                         / \
     *                        2    3
     *                       / \  / \
     *                     7    8 12 15
     *                             /
     *                            14
     *
     *
     * */
    // above diagram
    BinaryTree.Node root = new BinaryTree.Node(10);
    root.left = new BinaryTree.Node(2);
    root.right = new BinaryTree.Node(3);
    root.left.left = new BinaryTree.Node(7);
    root.left.right = new BinaryTree.Node(8);
    root.right.right = new BinaryTree.Node(15);
    root.right.left = new BinaryTree.Node(12);
    root.right.right.left = new BinaryTree.Node(14);
    System.out.println("Using  method 1");
    printLeftView(root);
    System.out.println("\nUsing other method.");
    leftView(root);
  }
}